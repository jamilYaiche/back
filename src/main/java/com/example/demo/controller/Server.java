package com.example.demo.controller;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;



public class Server extends Thread {
	private ServerSocket ss;
	private PrintWriter pw;
	private static boolean isRunning;
	
	
public void run() {
		
		try {
			ss = new ServerSocket(4442);
			
			System.out.println("Server is Running ...");
			isRunning = true;
            while (isRunning) {
			Socket s =ss.accept();
				new Conversation(s).start();
				
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}}

		class Conversation extends Thread{
			private Socket socket;
			
			
			public Conversation(Socket socket)
			{
				super();
				this.socket=socket;
			}
			
			public void run() {
				
				
				long rep;
			    
				
				try {
					  
					
					InputStream is = socket.getInputStream();
					InputStreamReader isr=new InputStreamReader(is);
					BufferedReader br = new BufferedReader(isr);
					
					
					OutputStream os = socket.getOutputStream();
				    pw = new PrintWriter(os , true);
					
					rep = Long.parseLong(br.readLine());
					
					
					if (rep==1)
					{
						
						
						this.socket.close();	
						
					}
					else if (rep==2)
					{
						
						
						
						this.socket.close();

					} 
					
					
					
								
												
				} //fin try 
				catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			
			}//fin run 
			
		}//fin conversation 
		
	
	
		
		
		
		
		
		
		
		

		
		
		public void Close(){
	        // TODO Auto-generated method stub         
	        isRunning = false;
	        InetAddress adress;
	        
	        try {
	            adress = InetAddress.getByName("localhost");
	            Socket socket = new Socket (adress, 4442);         
	        } catch (UnknownHostException e1) {
	            // TODO Auto-generated catch block
	            e1.printStackTrace();
	        } catch (IOException e2) {
	            // TODO Auto-generated catch block
	            e2.printStackTrace();
	        }
	        System.out.println("Serveur arrêté");
	         
	    }
		

	
}
