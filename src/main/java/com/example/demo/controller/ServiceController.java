package com.example.demo.controller;
import com.example.demo.model.*;

import com.example.demo.mysql.ServiceRepository;



import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
@CrossOrigin("*")
@RestController
public class ServiceController {
    @Autowired
    ServiceRepository repository;
    

	@RequestMapping(value="/reserver/{id}",method=RequestMethod.GET)
	public long getServ(@PathVariable Long id)
	{
		Client.send(String.valueOf(6+id));
		 
		return Long.parseLong(Client.read());
	}
	@RequestMapping(value="/getServiceByName/{name}",method=RequestMethod.GET)
	public List<Service> getServName(@PathVariable String name)
	{
		
		return repository.getServByName(name);
	}
   
    
		
		@RequestMapping(value="/getServByCateg/{catg}",method=RequestMethod.GET)
		public List <Service> getServ(@PathVariable String catg)
		{
			return repository.getServByCateg(catg);
		}

		@RequestMapping(value="/categories",method=RequestMethod.GET)
		public List <String> cat()
		
		{
			
			return repository.chercher();
		}
		@RequestMapping(value="/newServ",method=RequestMethod.GET)
		public String newServ()
		{
			
			repository.save(new Service ("Services finance","payment facture STEG"));
			repository.save(new Service ("Services finance","payment facture SONED"));
			repository.save(new Service ("Services administratifs","administraion STEG"));
			return "done";
		}
		@RequestMapping(value="/addServ/{cat}/{nom}", method=RequestMethod.GET)
		public Service addOneServ (@PathVariable String nom ,@PathVariable String cat  ) {
			
			Service s = new Service(nom,cat);
		
			
			repository.save(s);
			return s;
		}
		
		@RequestMapping(value="/services/delete", method=RequestMethod.GET) 
		public void deleteServices ()
		{
			repository.deleteAll();
		}
		@RequestMapping(value="/services/delete/{id}", method=RequestMethod.GET) 
		public void deleteServ (@PathVariable Long id)
		{
		
		 repository.deleteById(id);
		}
		@RequestMapping(value="/services/{id}", method=RequestMethod.GET) 
		public Optional<Service> findUser (@PathVariable Long id)
		{
		
		return repository.findById(id);
		}
		@RequestMapping(value="/services", method=RequestMethod.GET) 
		public List <Service> findServ ()
		{
		
		return repository.findAll();
		}
		@RequestMapping(value="/addServ", method=RequestMethod.POST ,
				consumes = {
						MediaType.APPLICATION_JSON_VALUE
				})
		public void createServ(@RequestBody Service s)
		{
			repository.save(s);
			
		}
		
		
	}
	
	 
	  


