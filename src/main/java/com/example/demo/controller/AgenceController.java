package com.example.demo.controller;


import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import com.example.demo.model.Agence;
import com.example.demo.mysql.AgenceRepository;

@CrossOrigin("*")
@RestController
public class AgenceController {
    @Autowired
    AgenceRepository repository;
    @RequestMapping(value="/newAg",method=RequestMethod.GET)
	public String newAg()
	{
    	
    	
		repository.save(new Agence (repository.getEta(1),"poste beb bhar ","beb bhar sfax",3000,"74 000 000","00","00","ex@mail.com"));
		repository.save(new Agence (repository.getEta(2),"STEG Sekit ezzit ","rte tunis km 6 sakiet ezzit sfax",3021,"74 000 000","00","00","ex@mail.com"));
		repository.save(new Agence (repository.getEta(1),"poste Nasria ","Nasria sfax",3000,"74 000 000","00","00","ex@mail.com"));
		return "done";
	}

		
		@RequestMapping(value="/addAgence/{ag}/{nom}/{adresse}/{cp}/{tel}/{mail}/{alt}/{lng}", method=RequestMethod.GET)
		public Agence addOneag (@PathVariable long ag ,@PathVariable String nom ,@PathVariable String adresse , @PathVariable int cp ,@PathVariable String tel ,@PathVariable String mail ,@PathVariable String alt, @PathVariable String lng  ) {
			Agence a = new Agence(repository.getEta(ag),nom, adresse, cp,  tel,  alt, lng, mail);
			
			repository.save(a);
			return a;
		}
		
		@RequestMapping(value="/agences/delete", method=RequestMethod.GET) 
		public void deleteAgenecy ()
		{
			repository.deleteAll();
		}
		@RequestMapping(value="/agences/delete/{id}", method=RequestMethod.GET) 
		public void deleteUser (@PathVariable long id)
		{
		
		 repository.deleteById(id);
		}
		@RequestMapping(value="/agences/{id}", method=RequestMethod.GET) 
		public Agence findAg (@PathVariable long id)
		{
		
		return repository.getAg(id);
		}
	
		@RequestMapping(value="/agences/service/{serv}", method=RequestMethod.GET) 
		public Optional<Agence> findUser (@PathVariable String serv)
		{
			
		return repository.findById((long)1);
		}
		@RequestMapping(value="/agences", method=RequestMethod.GET) 
		public List <Agence> findall ()
		{
		
		return repository.findAll();
		}
		@RequestMapping(value="/ags", method=RequestMethod.GET) 
		public List <String> getall ()
		{
		
		return repository.getAll();
		}
		@RequestMapping(value="/addAgence", method=RequestMethod.POST ,
				consumes = {
						MediaType.APPLICATION_JSON_VALUE
				})
		public void createUser(@RequestBody Agence u)
		{
			repository.save(u);
			
		}
		@RequestMapping(value="/agences/eta/{e}", method=RequestMethod.GET) 
		public List<Agence> findAgs (@PathVariable String e)
		{
		
		return repository.getAgs(repository.getEtaByName(e));
		}
	}