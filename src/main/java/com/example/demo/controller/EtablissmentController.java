package com.example.demo.controller;


import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Agence;
import com.example.demo.model.Etablissement;
import com.example.demo.model.User;
import com.example.demo.mysql.EtablissementRepository;


@CrossOrigin("*")
@RestController
public class EtablissmentController {
    @Autowired
    EtablissementRepository repository;
    
    @RequestMapping(value="/newEta", method=RequestMethod.GET)
	public String newEta () {
	
		repository.save(new Etablissement("La Poste Tunisienne"));
		repository.save(new Etablissement("STEG"));
		repository.save(new Etablissement("SONEDE"));
		return "done";
	}
    

		@RequestMapping(value="/addEta/{nom}", method=RequestMethod.GET)
		public String addEta (@PathVariable String nom) {
		
			repository.save(new Etablissement(nom));
			return "done";
		}
		@RequestMapping(value="/etablissement/{id}/makeSiege/{siege}", method=RequestMethod.GET)
		public String putSiege (@PathVariable long id,@PathVariable long siege) {
		
			repository.setSiege(id, repository.getAg(siege));
			return "done";
		}
		@RequestMapping(value="/etablissements", method=RequestMethod.GET) 
		public List <Etablissement> findEta ()
		{
		
		return repository.findAll();
		}
		@RequestMapping(value="/etas", method=RequestMethod.GET) 
		public List <String> findEtas ()
		{
		
		return repository.getAll();
		}
		
		
	}