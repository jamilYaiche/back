 package com.example.demo.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.User;

public interface UserRepository extends JpaRepository <User, Long> 
 {
	@Query("SELECT u FROM User u Where u.mail = :mail" ) 
	User findUserByMail(String mail);
	
   // @Query("select nom from user") 
	//public List <User> chercher();

 }