package com.example.demo.mysql;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Agence;
import com.example.demo.model.Etablissement;



public interface EtablissementRepository extends JpaRepository <Etablissement, Long> 
{
	@Query("SELECT e FROM Etablissement e Where e.numEta = :num" ) 
   Etablissement getEta(@Param("num") long id);
	@Query("SELECT a FROM Agence a Where a.numAgence = :num" ) 
    Agence getAg(@Param("num") long id);
	//modifing and Transactional are required de execute an update/delete querry
	@Modifying
	@Transactional
	@Query("UPDATE Etablissement e SET e.siege = :siege Where e.numEta = :num" ) 
    void setSiege(@Param("num") long id,@Param("siege") Agence a);
	@Query("SELECT e.nom FROM Etablissement e " ) 
	   List <String> getAll();
 
}