package com.example.demo.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Agence;
import com.example.demo.model.FilAttente;
import com.example.demo.model.Service;

public interface FilAttenteRepository extends JpaRepository <FilAttente, Long> 
{
	@Query("SELECT a FROM Agence a Where a.numAgence = :num" ) 
    Agence getAg(@Param("num") long id);
	@Query("SELECT s FROM Service s Where s.numServ = :num" ) 
	Service getsrv(@Param("num") long id);
	@Query("SELECT f.agence FROM FilAttente f Where f.service = :serv" ) 
	List <Agence> getAgsByServ(@Param("serv") Service serv);
	@Query("SELECT f.service FROM FilAttente f Where f.agence = :ags" ) 
	List <Service> getSrvByAgs(@Param("ags") Agence agence);
}