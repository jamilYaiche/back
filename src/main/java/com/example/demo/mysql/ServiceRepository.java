 package com.example.demo.mysql;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Agence;
import com.example.demo.model.Service;

public interface ServiceRepository extends JpaRepository <Service, Long> 
 {
    @Query("select distinct categorie from Service") 
	public List <String> chercher();
    
    @Query("SELECT s FROM Service s where s.categorie = :cat") 
    List <Service> getServByCateg(@Param("cat") String categorie);
    
    @Query("SELECT s FROM Service s where s.nom like %:name%") 
    List <Service> getServByName(@Param("name") String nom);
    @Query("SELECT s FROM Service s Where s.numServ = :num" ) 
    Service getsrv(@Param("num") long id);

 }