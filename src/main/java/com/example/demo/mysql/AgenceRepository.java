package com.example.demo.mysql;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Agence;
import com.example.demo.model.Etablissement;
import com.example.demo.model.FilAttente;


public interface AgenceRepository extends JpaRepository <Agence, Long> 
 {
	@Query("SELECT a FROM Agence a Where a.numAgence = :num" ) 
    Agence getAg(@Param("num") long id);
	@Query("SELECT a FROM Agence a Where a.eta = :etab" ) 
    List<Agence> getAgs(@Param("etab") Etablissement e);
	@Query("SELECT e FROM Etablissement e Where e.numEta = :num" ) 
	   Etablissement getEta(@Param("num") long id);
	@Query("SELECT e FROM Etablissement e Where e.nom = :name" ) 
	   Etablissement getEtaByName(@Param("name") String nom);
	@Query("SELECT a.nomAgence FROM Agence a " ) 
	   List <String> getAll();
	
 }