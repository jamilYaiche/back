package com.example.demo.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class FilAttente implements Serializable {
@Id
@GeneratedValue(strategy=GenerationType.IDENTITY)
private Long numFil ;
private int numeroInst;
private int dernierNum;


@ManyToOne(cascade= CascadeType.ALL)
private Agence agence;
@ManyToOne(cascade= CascadeType.ALL)
private Service service;




public Long getnumFil() {
	return numFil;
}
public void setnumFil(Long numFil) {
	this.numFil = numFil;
}
public int getNumeroInst() {
	return numeroInst;
}
public void setNumeroInst(int numeroInst) {
	this.numeroInst = numeroInst;
}
public int getDernierNum() {
	return dernierNum;
}
public void setDernierNum(int dernierNum) {
	this.dernierNum = dernierNum;
}

public Service getService() {
	return service;
}
public void setService (Service service) {
	this.service = service;
}


public FilAttente( Agence agence, Service service) {
	super();
	
	this.numeroInst = 0;
	this.dernierNum = 0;
	this.agence = agence;
	this.service = service;
	
}
public Long getNumFil() {
	return numFil;
}
public void setNumFil(Long numFil) {
	this.numFil = numFil;
}
public FilAttente() {
	super();
	// TODO Auto-generated constructor stub
}
public Agence getAgence() {
	return agence;
}
public void setAgence(Agence agence) {
	this.agence = agence;
}




}
