package com.example.demo.model;


import java.io.Serializable;
import java.util.Collection;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
@Entity
public class Agence implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long numAgence;
	private String nomAgence ;
	@OneToOne
	private Etablissement eta;
	private String adresse ;
	private int cp;
	@OneToMany(mappedBy= "agence") 
	private Collection <SeanceTravail> seances;
	private String tel;
	private String alt;
	private String lng;
	private Boolean active ;

	
	
	
	public Collection<SeanceTravail> getSeances() {
		return seances;
	}
	public void setSeances(Collection<SeanceTravail> seances) {
		this.seances = seances;
	}
	public Boolean getActive() {
		return active;
	}
	public void setActive(Boolean active) {
		this.active = active;
	}
	private String mail;
	
	public Agence() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Agence(Etablissement eta ,String nomAgence, String adresse, int cp, String tel, String alt, String lng,
			String mail) {
		super();
		
		this.nomAgence = nomAgence;
		this.adresse = adresse;
		this.cp = cp;
		this.tel = tel;
		this.alt = alt;
		this.lng = lng;
		this.mail = mail;
		this.active= false;
	}
	public Long getNumAgence() {
		return numAgence;
	}
	public void setNumAgence(Long numAgence) {
		this.numAgence = numAgence;
	}
	public String getNomAgence() {
		return nomAgence;
	}
	public void setNomAgence(String nomAgence) {
		this.nomAgence = nomAgence;
	}
	public Etablissement getEta() {
		return eta;
	}
	public void setEta(Etablissement eta) {
		this.eta = eta;
	}
	public String getAdresse() {
		return adresse;
	}
	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}
	public int getCp() {
		return cp;
	}
	public void setCp(int cp) {
		this.cp = cp;
	}
	public String getTel() {
		return tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}
	public String getAlt() {
		return alt;
	}
	public void setAlt(String alt) {
		this.alt = alt;
	}
	public String getLng() {
		return lng;
	}
	public void setLng(String lng) {
		this.lng = lng;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}

	 
	

}
