package com.example.demo.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class Service implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long numServ ;
	private String nom;
	private String categorie;
	
	public Long getNumServ() {
		return numServ;
	}
	public void setNumServ(Long numServ) {
		this.numServ = numServ;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	
	public Service() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Service(String cat,String nom) {
		super();
		
		this.nom = nom;
		this.categorie=cat;
		
		
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
	

	
}
