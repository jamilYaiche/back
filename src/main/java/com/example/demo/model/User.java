package com.example.demo.model;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
@Entity
public class User implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long numUser ;
	private String nom ;
	private String prenom ;
	private String numTel ;
	private String mail ;
	private String mdp ;
	private String type;
	@OneToMany(mappedBy= "user") 
	private Collection <Reservation> reservations;
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}
	public User( String nom, String prenom, String numTel, String mail, String mdp, String type) {
		super();
		
		this.nom = nom;
		this.prenom = prenom;
		this.numTel = numTel;
		this.mail = mail;
		this.mdp = mdp;
		this.type = type;
	}
	public Long getNumUser() {
		return numUser;
	}
	public void setNumUser(Long numUser) {
		this.numUser = numUser;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNumTel() {
		return numTel;
	}
	public void setNumTel(String numTel) {
		this.numTel = numTel;
	}
	public String getMail() {
		return mail;
	}
	public void setMail(String mail) {
		this.mail = mail;
	}
	public String getMdp() {
		return mdp;
	}
	public void setMdp(String mdp) {
		this.mdp = mdp;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
	

}
