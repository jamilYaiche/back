package com.example.demo.model;

import java.io.Serializable;

import java.sql.Time;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import org.springframework.format.datetime.standard.DateTimeContext;
@Entity
public class Reservation  implements Serializable {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long CodeReservation ;
	@ManyToOne(cascade= CascadeType.ALL)
	private User user ;
	@ManyToOne(cascade= CascadeType.ALL)
	private FilAttente fil;
	private Time heur;
	private int numTicket;
	public Long getCodeReservation() {
		return CodeReservation;
	}
	public void setCodeReservation(Long codeReservation) {
		CodeReservation = codeReservation;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public FilAttente getFil() {
		return fil;
	}
	public void setFil(FilAttente fil) {
		this.fil = fil;
	}
	public Time getHeur() {
		return heur;
	}
	public void setHeur(Time heur) {
		this.heur = heur;
	}
	public int getNumTicket() {
		return numTicket;
	}
	public void setNumTicket(int numTicket) {
		this.numTicket = numTicket;
	}
	public Reservation() {
		super();
		// TODO Auto-generated constructor stub
	}
	public Reservation(User user, FilAttente fil, Time heur, int numTicket) {
		super();
		this.user = user;
		this.fil = fil;
		this.heur = heur;
		this.numTicket = numTicket;
	}
	
	
	
	
	

}
