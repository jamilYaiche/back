package com.example.demo.model;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Time;
import java.util.Collection;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
@Entity
public class SeanceTravail  implements Serializable  {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long numSeance ;
	
	@ManyToOne(cascade= CascadeType.ALL)
	private Agence agence ;
	
	private boolean avtive ;
	private Date dateDeb;
	private Date dateFin; 
	private String jourDeb;
	private String jourFin;
	private Time heurDeb;
	private Time heurFin;
	public Long getNumSeance() {
		return numSeance;
	}
	public void setNumSeance(Long numSeance) {
		this.numSeance = numSeance;
	}
	public Agence getAgence() {
		return agence;
	}
	public void setAgence(Agence agence) {
		this.agence = agence;
	}
	public boolean isAvtive() {
		return avtive;
	}
	public void setAvtive(boolean avtive) {
		this.avtive = avtive;
	}
	public Date getDateDeb() {
		return dateDeb;
	}
	public void setDateDeb(Date dateDeb) {
		this.dateDeb = dateDeb;
	}
	public Date getDateFin() {
		return dateFin;
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}
	public String getJourDeb() {
		return jourDeb;
	}
	public void setJourDeb(String jourDeb) {
		this.jourDeb = jourDeb;
	}
	public String getJourFin() {
		return jourFin;
	}
	public void setJourFin(String jourFin) {
		this.jourFin = jourFin;
	}
	public Time getHeurDeb() {
		return heurDeb;
	}
	public void setHeurDeb(Time heurDeb) {
		this.heurDeb = heurDeb;
	}
	public Time getHeurFin() {
		return heurFin;
	}
	public void setHeurFin(Time heurFin) {
		this.heurFin = heurFin;
	}
	public SeanceTravail() {
		// TODO Auto-generated constructor stub
	}
	public SeanceTravail(Agence agence, boolean avtive, Date dateDeb, Date dateFin, String jourDeb, String jourFin,
			Time heurDeb, Time heurFin) {
		super();
		this.agence = agence;
		this.avtive = avtive;
		this.dateDeb = dateDeb;
		this.dateFin = dateFin;
		this.jourDeb = jourDeb;
		this.jourFin = jourFin;
		this.heurDeb = heurDeb;
		this.heurFin = heurFin;
	}
	
	
	
}
