package com.example.demo.model;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import java.util.Collection;
@Entity
public class Etablissement implements Serializable {
	@Id
	@GeneratedValue(strategy = GenerationType.TABLE)
	private Long numEta ;
	private String nom ;
	@OneToOne(cascade = CascadeType.ALL)
	private Agence siege ;
	
	public Long getNumEta() {
		return numEta;
	}
	public void setNumEta(Long numEta) {
		this.numEta = numEta;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public Agence getSiege() {
		return siege;
	}
	public void setSiege(Agence siege) {
		this.siege = siege;
	}
	
	public Etablissement( String nom) {
		super();
		
		this.nom = nom;
		
	}
	public Etablissement() {
		super();
		// TODO Auto-generated constructor stub
	}
	
}
